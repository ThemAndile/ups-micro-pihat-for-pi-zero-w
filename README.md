# UPS Micro PiHAT For Pi Zero W

## Description
The Uninterrupted Power Supply (UPS Micro PiHAT) attaches to the Raspberry Pi Zero W, and intelligently supplies power to it, so that it
it remains uninterrupted during power interruptions.

## What it is for?
The project is for the design of a UPS Micro PiHAT for the eee3088f course.

## How it will be of use?
- The PiHAT will be useful for those using the Raspberry Pi Zero W, for their projects.
- They won't lose their work, when power from grid is unavailable.
- PiHAT kicks in, and at full capacity powers Pi for around 3 hours.

## How it works?
The UPS Micro PiHAT uses 3 subsystems.

1. Voltage regulator
	- Uses a buck-boost conventer(LTC3112).
	- Takes voltage from a 6V rechargeable battery
	- and outputs approximately a regulated 5.1V.
2. Amplifier
	- Takes 5.1V and supplies correct voltages to the Pi,
	- using the an amplifier(AD8030).
3. Status LEDs
	- Uses battery voltage for power,
	- and indicates how the battery voltage is varying,
	- with 3 Red LEDs(NSCW100),
	- in a voltage battey level indicator.

 


